class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_hash
      t.string :password_salt 
      t.string :secret_token      
      t.boolean :is_admin,     :default => false
      t.boolean :is_confirmed, :default => false
      t.datetime :confirmed_at

      t.string :first_name
      t.string :last_name
      t.string :country
      t.string :region_or_district      
      t.string :account_type
      t.string :account_sub_type
      t.string :company_name
      t.string :map_market
      t.string :address
      t.string :post_code
      t.string :city
      t.string :phone_number
      t.boolean :user_agreement_policy, :default => false
      t.boolean :receive_communication, :default => false
      t.boolean :user_above_18,         :default => false

      t.timestamps
    end
  end
end
