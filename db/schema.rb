# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150524185059) do

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "secret_token"
    t.boolean  "is_admin",              default: false
    t.boolean  "is_confirmed",          default: false
    t.datetime "confirmed_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "country"
    t.string   "region_or_district"
    t.string   "account_type"
    t.string   "account_sub_type"
    t.string   "company_name"
    t.string   "map_market"
    t.string   "address"
    t.string   "post_code"
    t.string   "city"
    t.string   "phone_number"
    t.boolean  "user_agreement_policy", default: false
    t.boolean  "receive_communication", default: false
    t.boolean  "user_above_18",         default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
