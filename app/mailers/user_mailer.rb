class UserMailer < ActionMailer::Base

  default from: "info@example.com"
  # with delayed_job
  #Notifier.delay.signup(@user)


  def send_confirm_email(user)
    @user = user
    mail to: user.email, subject: "Welcome to Buneyard Compagnie!"
  end

  def create_wallet(user)
    @user = User.find(user.id)
    mail to: @user.email, subject: "SalvadorCoins Wallet has been created!"
  end

  def deduct_coin_amount(process_id)
    @process_payment = ProcessPayment.find_by_process_id(process_id)
    @user = User.find(@process_payment.sender_id)
    mail to: @user.email, subject: "SalvadorCoins Wallet Transaction!"
  end

  def recive_coin_amount(process_id)
    @process_payment = ProcessPayment.find_by_process_id(process_id)
    @user = User.find(@process_payment.reciver_id)
    mail to: @user.email, subject: "SalvadorCoins Wallet Transaction!"
  end

end