class User < ActiveRecord::Base

  #has_one :custom_user_detail

  validates :email, uniqueness: true
  
  attr_accessor :password, :password_confirmation, :phone_tmp_token
  #attr_accessible :password, :email, :password_hash, :password_salt, :secret_token, :kyc_secret_token, :is_admin
  
  before_create :encrypt_password  
  before_create :generate_token

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      #session[:user_id] = user.id      
      user
    else
      nil
    end
  end

  def encrypt_password
    require 'securerandom'      
    if self.password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
      #self.kyc_secret_token = SecureRandom.hex(10)              
      self.secret_token = SecureRandom.hex(10)      
    end
  end
  def generate_token
     require 'securerandom'    
     self.phone_no_token = SecureRandom.hex(3) 
  end
end