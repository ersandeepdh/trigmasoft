Rails.application.routes.draw do  

  #get 'users/new'

  # get 'salvador_coins_pool/salvador_coins_transaction'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  #root 'salvador_coins_pool#index'
  #root 'welcome#index'
  
  get '/msg' => 'welcome#send_msg'#, :as => 'send_msg'
  post '/msg' => 'welcome#send_msg', :as => 'send_msg'
  # get '/sign-in' => 'users#sign_in'

  # post '/sign-in' => 'users#sign_in', :as => "users_sign_in"
  # post "/register" => "users#create", :as => "users_create"

  # get '/forgot-password' => 'users#forgot_password'
  # post '/forgot-password' => 'users#forgot_password', :as => "forgot_passwd"

  # get '/user-profile/:secret_token' => "users#profile", :as => "users_profile"
  # patch '/user-profile/:secret_token' => 'users#profile' , :as=>'users_profile_post'

  # get '/user-profile-page/:secret_token' => 'users#full_profile' , :as=>'full_profile'

  # get '/confirm-user-account/:secret_token' => "users#confirm"  
  # get '/profile/:id'=>'users#profile'
  
  # get '/confirm-phone/:secret_token' => 'users#confirm_phone', :as => 'users_confirm_phone'
  # patch '/confirm-phone/:secret_token' => 'users#confirm_phone' , :as => 'confirm_phone_post'

  # get '/verify-phone/:secret_token' => 'users#verify_phone', :as => 'verify_phone' 
  # patch '/verify-phone/:secret_token' => 'users#verify_phone',:as => 'verify_phone_post'

  # post  '/my_profile'=>'users#my_profile'
  # get 'v1/bankbalance' => 'salvador_coins_pool#bankbalance'
  # get 'v1/getrecivedamount' => 'salvador_coins_pool#getrecivedamount'
  # get 'v1/purchase/:process_id/amount/:amount/wallet/:wallet/user_id/:user_id' => 'salvador_coins_pool#purchase'  

  # # Example of regular route:
  # get 'v1/accounts/:source_address/:amount/payment/:destination_address/:process_id' => 'salvador_coins_pool#coins_transaction'                        
  # #get 'v1/accounts/:source_address/payment/:destination_address/:amount' => 'salvador_coins_pool#coins_transaction'
  # get 'v1/wallet-setup/:user_secret_key' => 'salvador_coins_pool#create_wallet'
  

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
